<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user/add','UserController@create_user');

Route::middleware(['auth:api'])->group(function () {
    Route::get('user/get','UserController@read_user');
    Route::put('user/update','UserController@update_user');
    Route::delete('user/delete','UserController@delete_user');

    Route::post('trip/add','TripsController@create_trip');
    Route::get('trip/get','TripsController@read_trips_by_args');
    Route::post('trip/update','TripsController@update_trip');
    Route::delete('trip/delete','TripsController@delete_trip');
    
    Route::post('booking/add','BookingController@create_booking');
    Route::get('booking/get','BookingController@read_booking_by_args');
    Route::put('booking/update','BookingController@update_booking');
    Route::delete('booking/delete','BookingController@delete_booking');
});


