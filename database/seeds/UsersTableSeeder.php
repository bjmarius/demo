<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Carbon\CarbonTimeZone;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            array(
                [
                    'id' => 1,
                    'first_name' => 'admin',
                    'last_name' => 'test',
                    'email' => 'admin@core.loc',
                    'password' => Hash::make('admin@123'),
                    'is_admin' => 1
                ]
            )
        );
    }
}
