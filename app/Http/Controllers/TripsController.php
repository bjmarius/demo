<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

use  App\Http\Controllers\Controller;

Use Validator;
use App\Models\Trip;

class TripsController extends Controller
{
    public function create_trip(Request $request){
        $createTrip = new Trip();
        if(isset($request->title) && $request->title!=""){
            $createTrip->slug = Str::slug($request->title);
            $createTrip->title = $request->title;
        }
        if(isset($request->description) && $request->description!=""){
            $createTrip->description = $request->description;
        }
        if(isset($request->start_date) && $request->start_date!=""){
            $createTrip->start_date = date('Y-m-d',strtotime($request->start_date));
        }
        if(isset($request->end_date) && $request->end_date!=""){
            $createTrip->end_date = date('Y-m-d',strtotime($request->end_date));
        }
        if(isset($request->location) && $request->location!=""){
            $createTrip->location = $request->location;
        }
        if(isset($request->price) && $request->price!=""){
            $createTrip->price = $request->price;
        }

        $messages = [];
        $rules = ['slug' => 'required|unique:trips|max:255'];
        $validationArray = array('slug' => $createTrip->slug);
        $validator = Validator::make($validationArray, $rules, $messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()], 422);
        } else {
            $createTrip->save();
            if(isset($createTrip->id)){
                return response()->json(['message' => "Trip created!"], 200);
            }else{
                return response()->json(['message' => "Trip faied to be created!"], 422);
            }
        }
    }
    public function read_trips_by_args(Request $request){
        $whereArray = array();
        if(isset($request->slug) && $request->slug != ""){
            $whereArray["slug"] = $request->slug;
        }
        if(isset($request->start_date) && $request->start_date != ""){
            $whereArray["start_date"] = $request->slug;
        }
        if(isset($request->end_date) && $request->end_date != ""){
            $whereArray["end_date"] = $request->end_date;
        }
        if(count($whereArray)>0){
            $getResults = Trip::where($whereArray);
        }else{
            $getResults = Trip::whereRaw('1 = 1');
        }

        if(isset($request->start_date_from) && $request->start_date_from != ""){
            $getResults->whereDate('start_date','>=',$request->start_date_from);
        }
        if(isset($request->end_date_to) && $request->end_date_to != ""){
            $getResults->whereDate('end_date','<=',$request->end_date_to);
        }
        if(isset($request->price_from) && $request->price_from != "" && floatval($request->price_from)>0){
            $getResults->whereDate('price','>=',$request->price_from);
        }
        if(isset($request->price_to) && $request->price_to != ""  && floatval($request->price_to)>0){
            $getResults->whereDate('price','<=',$request->price_to);
        }
        if(isset($request->order_by) && is_array($request->order_by) && count($request->order_by)>0){
            foreach($request->order_by as $orderByKey=>$orderByDirection){
                $getResults->orderBy($orderByKey,$orderByDirection);
            }
        }
        return response()->json($getResults->get(), 200);
    }
    public function update_trip(Request $request){
        if(isset($request->id)){
            $trip = Trip::find($request->id);
            if($trip){
                $updateArr = array();
                if(isset($request->title) && $request->title!=""){
                    $updateArr["slug"] = Str::slug($request->title);
                    $updateArr["title"] = $request->title;
                }
                if(isset($request->description) && $request->description!=""){
                    $updateArr["description"] = $request->description;
                }
                if(isset($request->start_date) && $request->start_date!=""){
                    $updateArr["start_date"] = date('Y-m-d',strtotime($request->start_date));
                }
                if(isset($request->end_date) && $request->end_date!=""){
                    $updateArr["end_date"] = date('Y-m-d',strtotime($request->end_date));
                }
                if(isset($request->location) && $request->location!=""){
                    $updateArr["location"] = $request->location;
                }
                if(isset($request->price) && $request->price!=""){
                    $updateArr["price"] = $request->price;
                }
                if(isset($updateArr["slug"])){
                    $messages = [];
                    $rules = ['slug' => 'required|unique:trips|max:255'];
                    $validationArray = array('slug' => $updateArr["slug"]);
                    $validator = Validator::make($validationArray, $rules, $messages);
                }
                if (isset($validator) && $validator->fails()) {
                    return response()->json(['errors' => $validator->messages()], 422);
                } else {
                    $trip->update($updateArr);
                    return response()->json(['message' => "Trip updated!"], 200);
                }
            }
        }
        return response()->json(['message' => "Trip not found!"], 422);
    }
    public function delete_trip(Request $request){
        if(isset($request->id)){
            $trip = Trip::find($request->id);
            if($trip){
                $trip->delete();
                return response()->json(['message' => "Trip deleted!"], 200);
            }else{
                return response()->json(['message' => "Trip not found!"], 422);
            }
        }else{
            return response()->json(['message' => "Trip not found!"], 422);
        }
    }
}

