<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

Use Validator;
use App\User;

class UserController extends Controller
{
    public function create_user(Request $request){
        $messages = [];

        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required'
        ];
        $validationArray = array(
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => $request->password
        );
        $validator = Validator::make($validationArray, $rules, $messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()], 422);
        } else {
            $user = new User();
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            if(isset($user->is_admin)){
                $user->is_admin = $request->is_admin;
            }
            $user->save();
        }
        return response()->json(['message' => "User created!"], 200);
    }
    public function read_user(Request $request){
        $whereArray = array();
        if(isset($request->first_name) && $request->first_name != ""){
            $whereArray["first_name"] = $request->first_name;
        }
        if(isset($request->last_name) && $request->last_name != ""){
            $whereArray["last_name"] = $request->last_name;
        }
        if(isset($request->email) && $request->email != ""){
            $whereArray["email"] = $request->email;
        }
        if(isset($request->id) && $request->id != ""){
            $whereArray["id"] = $request->id;
        }
        if(count($whereArray)>0){
            $getResults = User::where($whereArray);
        }else{
            $getResults = User::whereRaw('1 = 1');
        }
        // if(isset($request->get_bookings)){
        //     $getResults->with("booking");
        // }
        return response()->json($getResults->get(), 200);
    }
    public function update_user(Request $request){
        $messages = [];

        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required'
        ];
        $validationArray = array(
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => $request->password
        );
        $validator = Validator::make($validationArray, $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()], 422);
        } else {
            if(isset($user->is_admin)){
                $validationArray["is_admin"] = $request->is_admin;
            }
            if(isset($request->password)) {
                $validationArray['password'] = Hash::make($request->password);
            }
            $user = User::where('id', $request->id)->first();
            if($user){
                $user->update($request);
                return response()->json(['message' => "User updated!"], 200);
            }else{
                return response()->json(['message' => "User not found!"], 422);
            }
        }
    }
    public function delete_user(Request $request){
        User::where('id', $request->id)->delete();
        return response()->json(['message' => "User deleted!"], 200);
    }
}