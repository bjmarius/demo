<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use  App\Http\Controllers\Controller;

use App\Models\Booking;
use App\Models\Trip;
use App\User;

class BookingController extends Controller
{
    public function create_booking(Request $request){
        if(isset($request->user_id) && $request->user_id!="" && isset($request->trip_id) && $request->trip_id!=""){
            $createBooking = new Booking();
            $createBooking->user_id = $request->user_id;
            $createBooking->trip_id = $request->trip_id;
            $createBooking->save();
            if(isset($createBooking->id)){
                return response()->json(['message' => "Booking created!"], 200);
            }else{
                return response()->json(['message' => "Booking faied to be created!"], 422);
            }
        }else{
            return response()->json(['message' => "user_id or trip_id not set! Please provide these parameters!"], 422);
        }
    }
    public function read_booking_by_args(Request $request){
        $whereArray = array();
        if(isset($request->id)){
            $whereArray["id"] = $request->id;
        }
        if(isset($request->user_id)){
            $whereArray["user_id"] = $request->user_id;
        }
        if(isset($request->trip_id)){
            $whereArray["trip_id"] = $request->trip_id;
        }
        if(isset($request->status)){
            $whereArray["status"] = $request->status;
        }

        if(count($whereArray)>0){
            $getResults = Booking::where($whereArray);
        }else{
            $getResults = Booking::whereRaw('1 = 1');
        }
        $getResults->with("trip");

        /*
        //******** no eloqvent ***
        $getResults = DB::table('bookings');
        foreach($whereArray as $whereItems=>$whereItemsValue){
            $getResults->where('bookings.'.$whereItems, $whereItemsValue);
        }
        $getResults->join('trips', 'bookings.trip_id', '=', 'trips.id');
        */
        return response()->json($getResults->get(), 200);
    }
    public function update_booking(Request $request){
        if(isset($request->id) && $request->id!=""){
            $booking = Booking::find($request->id);
            if($booking){
                $bookingUpdateArray = array();
                if(isset($request->user_id) && $request->user_id!=""){
                    $bookingUpdateArray["user_id"] = $request->user_id;
                }
                if(isset($request->trip_id) && $request->trip_id!=""){
                    $bookingUpdateArray["trip_id"] = $request->trip_id;
                }
                if(isset($request->status) && $request->status!=""){
                    $bookingUpdateArray["status"] = $request->status;
                }
                if(count($bookingUpdateArray)>0){
                    $booking->update($bookingUpdateArray);
                    return response()->json(['message' => "Booking updated!"], 200);
                }
            }
        }
        return response()->json(['message' => "Booking not found!"], 422);
    }
    public function delete_booking(Request $request){
        if(isset($request->id) && $request->id!=""){
            $booking = Booking::find($request->id);
            if($booking){
                $booking->delete();
                return response()->json(['message' => "Booking deleted!"], 200);
            }
        }
        return response()->json(['message' => "Booking not found!"], 422);
    }
}