<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use App;
Use App\User;
Use App\Models\Trip;

class Booking extends Model
{
    use Notifiable;

    protected $fillable = [
        'user_id','trip_id','status'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function trip(){
        return $this->belongsTo(Trip::class,'trip_id');
    }
}