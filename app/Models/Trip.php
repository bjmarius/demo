<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use App;
Use App\Models\Booking;

class Trip extends Model
{
    use Notifiable;
    
    protected $fillable = [
        'id','slug','title', 'description', 'start_date','end_date','location','price'
    ];

    public function booking(){
        return $this->hasMany(Booking::class,'id');
    }
}